//version inicial
//createClient

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3010;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json')

var path = require('path');
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/ogarcia/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;
var productMLabRaiz;
var movtosMLabRaiz;
var urlapiGeoLoc = "https://maps.googleapis.com/maps/api/staticmap"

var urlClientesMongo = "https://api.mlab.com/api/1/databases/ogarcia/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlProductosMongo = "https://api.mlab.com/api/1/databases/ogarcia/collections/Products?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlMovtosMongo = "https://api.mlab.com/api/1/databases/ogarcia/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var productoMLabPost = urlProductosMongo;

var clienteMLab = requestjson.createClient(urlClientesMongo);
var productoMLab = requestjson.createMovtos(urlProductosMongo);
var movtosMLab = requestjson.createMovtos(urlMovtosMongo);

app.listen(port);

var movimientosJSONv2 = require('.//movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

//recupera Detalle del Producto seleccionado de Mongo a traves de MLab - ok -
//por probar
app.get('/productos:IDProducto', function(req,res) {
  res.send("Detalle del producto|:" + req.params.IDProducto);
  productoMLab.get('',function(err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})


//recupera Productos de Mongo a traves de MLab - ok -
app.get('/productos', function(req,res) {
  productoMLab.get('',function(err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

// realiza un insert en MongoDB a traves de MLab - ok -
// 2019 oct 04
app.post('/productosadd', function(req, res) {
          res.header("Access-Control-Allow-Headers", "*");
          res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
          //declarar variables para armar el query que realizara el insert en la BD Mongo
          var productoid = req.body.productoid;
          var Nombre = req.body.name;
          var Descripcion = req.body.description;
          var Categoria = req.body.categoria;
          var PrecioUnitario = req.body.priceUnit;
          var Moneda = req.body.divisa;
          var Fabricante = req.body.manufacturer;
          var UnidadesStock = req.body.unitsInStock;
          var UnidadesporOrdenar = req.body.unitsInOrder;
          var Imagen =  "imagen9999.png";
          var TipoProducto = "Nuevo";
          console.log(TipoProducto);

          var query = '{"IDProducto":"' + productoid + '", "Nombre":"' + Nombre + '", "Descripcion":"' + Descripcion + '"", "Categoria":"' + Categoria  ;
          query = query + ', "PrecioUnitario": ' + PrecioUnitario + ', "Moneda":"' + Moneda + '", "Fabricante":"' + Fabricante + '", "UnidadesStock": ' + UnidadesStock ;
          query = query + ', "UnidadesporOrdenar": ' + UnidadesporOrdenar + ', "Imagen":"' + Imagen + '", "TipoProducto":"' + TipoProducto + '" }';
          console.log("query");
          console.log(query);

          console.log("req.body");
          console.log(req.body);
          console.log(" ");

          productoMLabPost = requestjson.createMovtos(urlProductosMongo);
          console.log(" requestjson productoMLabPost ");
          console.log(productoMLabPost);

          productoMLabPost.post('', req.body, function(err, resM, body){
            if (err) {
              console.log(body);
            } else {
              //res.send(query);
              res.send(body);
            }
          })
})
// 2019 oct 03

//peticion REST para login
app.post('/v2/Login', function(req, res) {
          res.header("Access-Control-Allow-Headers", "*");
          res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
          var email = req.headers.email;
          var password = req.headers.password;
          var query = 'q={"email":"'+ email + '", "password":"' + password + '"}';
          console.log(query);

          //clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
          movtosMLabRaiz = requestjson.createMovtos(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
          //console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);
          console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);

          //clienteMLabRaiz.get('', function(err, resM, body) {
          movtosMLabRaiz.get('', function(err, resM, body) {
            if (!err) {
              if (body.length > 0) {  //login OK
                res.status(200).send('Usuario logeado correctamente');
              } else {
                res.status(404).send('Usuario NO encontrado');
              }
            } else {
              console.log(body);
            }
      })
})

//funcion por default -probada OK-
app.get('/',function(req, res) {
  res.sendFile(path.join(__dirname,'index.html'));
} )
//funcion post -probada OK-
app.post('/', function(req, res) {
  res.send("Hemos recibido su petición Post");
})
//se agregan peticiones PUT y DELETE -probada OK-
app.put('/', function(req, res) {
  res.send("Hemos recibido su petición Put");
})
//se agregan peticiones PUT y DELETE -probada OK-
app.delete ('/', function(req, res) {
  res.send("Hemos recibido su petición DELETE");
})

//se agrega funcion para obtener los movimientos de la collección Movimientos -probada OK-
app.get('/Movtos',function(req,res) {
  res.send("Aqui tiene todos los Movimientos");
})

// igual al anterior-probada OK-
app.get('/Movtos/:idcliente',function(req,res) {
  res.send("Aqui tiene los movimientos del cliente:" + req.params.idcliente);
})

//recupera movimientos del archivo Json -probada OK-
app.get('/v1/Movtos', function(req, res) {
  res.sendfile('movimientosv1.json');
})
//recupera movimientos del archivo Json -probada OK-
app.get('/v2/Movtos', function(req, res) {
  res.json(movimientosJSONv2);
})



//para regresar un solo registro -a medias, pero salio-
//app.get('/v2/Movtos/:referencia/:email', function(req, res) {
app.get('/v2/Movtos/:idtrans', function(req, res) {
  console.log(req.params);
  console.log(req.params.idtrans);
  console.log(req.params.email);
  res.send(movimientosJSONv2[req.params.idtrans].email);
  //res.send(movimientosJSONv2[req.params.id-1].ciudad);
})

//nueva peticion get   -ok-
app.get('/v2/Movtosq', function(req, res) {
  console.log(req.query);
  res.send("Petición con query recibida desde Browser :" + req.query);
})



//recupera Movimientos de Mongo a traves de MLab - ok -
app.get('/v3/clientes', function(req,res) {
  clienteMLab.get('',function(err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})



//recupera Movimientos de Mongo a traves de MLab - ok -
app.get('/v3/Movtos', function(req,res) {
  movtosMLab.get('',function(err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

// realiza un insert en MongoDB a traves de MLab - ok -
app.post('/v3/Movtos', function(req, res) {
  movtosMLab.post('', req.body, function(err, resM, body){
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})
